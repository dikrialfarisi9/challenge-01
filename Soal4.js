function isValidPassword(password) {
  var karakter = /^(?=.*[\@\_\-\+\=\!\.\"\;\:\,\?\\])/;
  let cekKarakter = karakter.test(password);

  var hurufBesar = /^(?=.*[A-Z])/;
  let cekHurufBesar = hurufBesar.test(password);

  var hurufKecil = /^(?=.*[a-z])/;
  let cekHurufKecil = hurufKecil.test(password);

  var angka = /^(?=.*[0-9])/;
  let cekAngka = angka.test(password);

  var panjang = /^(?=.{8,})/;
  let cekPanjang = panjang.test(password);

  if (typeof password == "number") {
    return "Password Wajib String!!";
  } else if (password == null) {
    return "Password Tidak Boleh Kosong";
  } else if (cekKarakter) {
    return "Email tidak boleh ada karakter!";
  } else {
    var Regex = new RegExp("^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.{8,})");
    let cekPassword = Regex.test(password);
    if (cekPassword) {
      return "VALID";
    } else {
      if (!cekHurufBesar) {
        return "Tidak Terdapat Huruf Besar!";
      }
      if (!cekHurufKecil) {
        return "Tidak Terdapat Huruf Kecil!";
      }
      if (!cekAngka) {
        return "Tidak Terdapat Angka!";
      }
      if (!cekPanjang) {
        return "Panjang Kurang dari 8!";
      }
    }
  }
}

console.log(isValidPassword("Meong2021"));

console.log(isValidPassword("meong2021"));

console.log(isValidPassword("@eong"));

console.log(isValidPassword("Meong2"));

console.log(isValidPassword(0)); // Kenapa Error? Karena parameter yang dimasuka

console.log(isValidPassword());
