function changeWord(selectedText, changedText, text) {
  return text.replace(selectedText, changedText);
}

const kalimat1 = "Andini sangat mencintai kamu selamanya";
let kalimat2 = "";

kalimat2 = "Gunung bromo tak akan mampu manggambarkan besarnya cintaku padamu";

console.log(changeWord("mencintai", "membenci", kalimat1));

console.log(changeWord("bromo", "semeru", kalimat2));
