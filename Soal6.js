function getAngkaTerbesarKedua(dataNumber) {
  if (!Array.isArray(dataNumber)) {
    return "Error";
  } else if (dataNumber.length <= 1) {
    return "Error : 2 More";
  } else {
    dataNumber
      .sort((a, b) => {
        return a - b;
      })
      .reverse();

    let angkaTerbesar = Math.max(...dataNumber);

    let totalAngkaTebesar = dataNumber.filter(
      (dataNumber) => dataNumber == angkaTerbesar
    ).length;

    let angkaTerbesarKedua = 0;

    return dataNumber[angkaTerbesarKedua + totalAngkaTebesar];
  }
}

const dataAngka = [10, 10, 9, 11, 9, 4, 7, 7, 4, 3, 2, 2, 8];

console.log(getAngkaTerbesarKedua(dataAngka));

console.log(getAngkaTerbesarKedua(0));

console.log(getAngkaTerbesarKedua());
