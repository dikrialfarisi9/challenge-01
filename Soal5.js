function getSplitName(personeName) {
  let name = { firstname: null, middlename: null, lastname: null };

  let cek = personeName.split(" ");

  if (cek.length == 3) {
    name.firstname = cek[0];
    name.middlename = cek[1];
    name.lastname = cek[2];
  } else if (cek.length == 2) {
    name.firstname = cek[0];
    name.lastname = cek[1];
  } else if (cek.length == 1) {
    name.firstname = cek[0];
  } else if (cek.length > 3) {
    return "Error : This function is only for 3 characters name";
  }

  return name;
}

console.log(getSplitName("Aldi Daniela Pranata"));

console.log(getSplitName("Dwi Kuncoro"));

console.log(getSplitName("Aurora"));

console.log(getSplitName("Aurora Aureliya Sukma Darma"));

// console.log(getSplitName(0)); Kenapa Error? Karena Parameter yang di masukan harus berupa string!
