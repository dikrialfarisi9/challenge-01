const dataPenjualanNovel = [
  {
    idProduct: "BOOK002421",
    namaProduk: "Pulang - Pergi",
    penulis: "Tere Liye",
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduct: "BOOK002351",
    namaProduk: "Selamat Tinggal",
    penulis: "Tere Liye",
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduct: "BOOK002941",
    namaProduk: "Garis Waktu",
    penulis: "Fiersa Besari",
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduct: "BOOK002941",
    namaProduk: "Laskar Pelangi",
    penulis: "Andrea Hirata",
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];

function getInfoPenjualan(data) {
  let dataObj = {
    totalKeuntungan: null,
    totalModal: null,
    produkBukuTerlaris: null,
    persentaseKeuntungan: null,
    penulisTerlaris: null,
  };

  if (Array.isArray(data)) {
    let totalKeuntungan = 0;
    let totalModal = 0;

    for (let i = 0; i < data.length; i++) {
      if (typeof data[i] == "object") {
        totalKeuntungan +=
          (data[i].hargaJual - data[i].hargaBeli) * data[i].totalTerjual;
        totalModal +=
          data[i].hargaBeli * (data[i].sisaStok + data[i].totalTerjual);
      } else {
        return "Invalid data type";
      }
    }

    const totalTerjualMax = data.reduce(function (prev, cur) {
      return prev.totalTerjual > cur.totalTerjual ? prev : cur;
    });

    dataObj.totalKeuntungan = totalKeuntungan.toLocaleString("id-ID", {
      style: "currency",
      currency: "IDR",
    });
    dataObj.totalModal = totalModal.toLocaleString("id-ID", {
      style: "currency",
      currency: "IDR",
    });
    dataObj.persentaseKeuntungan = `${(
      (totalKeuntungan / totalModal) *
      100
    ).toFixed(2)} %`;
    dataObj.produkBukuTerlaris = totalTerjualMax.namaProduk;
    dataObj.penulisTerlaris = totalTerjualMax.penulis;
  }

  return dataObj;
}

console.log(getInfoPenjualan(dataPenjualanNovel));
