function checkEmail(email) {
  if (email != null) {
    var Regex = /^([a-z0-9\_\.]*)\@([a-z]*)(\.[a-z0-9]*)+$/;
    let cek = Regex.test(email);

    var regex2 = /^(?=.*[@])/;
    let cek2 = regex2.test(email);
    if (cek) {
      return "VALID";
    } else if (!cek2) {
      return "Email Wajib ada @";
    } else {
      return "INVALID";
    }
  } else {
    return "Mohon maaf email tidak boleh kosong";
  }
}

console.log(checkEmail("apranata@binar.co.id"));
console.log(checkEmail("apranata@binar.com"));
console.log(checkEmail("apranata@binar"));
console.log(checkEmail("apranata")); // Error tidak terdapat @ pada email yang diinputkan

// console.log(checkTypeNumber(checkEmail(3322))); // Error Karena tidak terdapat sebuah fungsi yang bernama checkTypeNumber

console.log(checkEmail()); // Error karena tidak ada email atau parameter yang di inputkan atau di masukkan
